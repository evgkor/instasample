#import <WebKit/WebKit.h>
#import "SessionService.h"

static NSString *const kAccessTokenKey = @"access_token";

@implementation SessionService

NSUserDefaults *store;

- (instancetype)init
{
    self = [super init];
    if (self) {
        store = [[NSUserDefaults alloc] initWithSuiteName:@"app.session.store"];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static SessionService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SessionService alloc] init];
        
    });
    return sharedInstance;
}

// TODO: Сохранить в Keychain
- (void) setAccessToken:(NSString *)accessToken {
    [store setValue:accessToken forKey:kAccessTokenKey];
    [store synchronize];
}

// TODO: Получить из Keychain
- (NSString*) accessToken {
    return [store objectForKey:kAccessTokenKey];
}

- (BOOL)isAuthorized {
    return self.accessToken != nil;
}

- (void)clearCookieStorage {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [fileManager URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask].firstObject;
    NSURL *fileURL = [url URLByAppendingPathComponent:@"/Cookies/Cookies.binarycookies"];
    NSError *error;
    [fileManager removeItemAtURL:fileURL error:&error];
}

@end
