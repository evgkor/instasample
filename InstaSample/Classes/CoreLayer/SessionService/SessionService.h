#import <Foundation/Foundation.h>

@interface SessionService : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic) NSString *accessToken;
@property (nonatomic) BOOL isAuthorized;

- (void)clearCookieStorage;

@end
