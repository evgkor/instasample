#import "ApplicationAssembly.h"
#import "MediaAssembly.h"

@implementation ApplicationAssembly

- (UIViewController *)rootController {
    MediaAssembly *assembly = [[MediaAssembly alloc] init];
    UIViewController *controller = [assembly createModule:nil];
    UIViewController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    return navController;
}

@end
