#import <UIKit/UIKit.h>

@interface ApplicationAssembly : NSObject 

@property (nonatomic, strong, readonly) UIViewController *rootController;

@end
