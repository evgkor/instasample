#import <AFNetworking/AFNetworking.h>
#import "MediaServiceImplementation.h"
#import "Constants.h"
#import "Endpoints.h"
#import "Helpers.h"
#import "SessionService.h"

@implementation MediaServiceImplementation

- (void)obtainMedia:(void (^)(NSArray <MediaRealmObject *> *media, NSError *error))completion {
    printFunction(_cmd);
    // TODO: Передать токен через адаптер
    NSString *urlString = [NSString stringWithFormat:@"%@%@?count=20&access_token=%@", kBaseURL, kEndpointMediaRecent, SessionService.sharedInstance.accessToken];
    NSLog(@"urlString: %@", urlString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSArray *dataFromResponse = [responseObject valueForKeyPath:@"data"];
        NSMutableArray *mutableData = [NSMutableArray arrayWithCapacity:[dataFromResponse count]];
        for (NSDictionary *attributes in dataFromResponse) {
            MediaRealmObject *media = [[MediaRealmObject alloc] initWithAttributes:attributes];
            [mutableData addObject:media];
        }
        completion([NSArray arrayWithArray:mutableData], nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        // TODO: Обработать ошибку
        // TODO: Обработать истечение токена
        completion([[NSArray alloc] init], error);
    }];
}

@end
