#import <Foundation/Foundation.h>
#import "MediaRealmObject.h"

@protocol MediaService <NSObject>

- (void)obtainMedia:(void (^)(NSArray <MediaRealmObject *> *media, NSError *error))completion;

@end
