#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "UserRealmObject.h"
#import "MediaRealmObject.h"
#import "MediaCollectionChange.h"

@protocol MediaCacheService <NSObject>

- (BOOL)isUserCached;
- (BOOL)isMediaCached;
- (UserRealmObject *)fetchUserFromCache;
- (RLMResults<MediaRealmObject *> *)fetchMediaFromCache;
- (void)writeUserToCache:(UserRealmObject *) user;
- (void)writeMediaToCache:(NSArray<MediaRealmObject *> *) media;
- (void)updateMediaCache:(NSArray<MediaRealmObject *> *) media;

@end
