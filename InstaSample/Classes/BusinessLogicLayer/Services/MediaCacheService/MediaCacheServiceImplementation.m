#import <Realm/Realm.h>
#import "MediaCacheServiceImplementation.h"
#import "Realm+NewRealmInstance.h"
#import "UserRealmObject.h"

@implementation MediaCacheServiceImplementation

- (BOOL)isUserCached {
    return [self fetchUserFromCache] != nil;
}

- (BOOL)isMediaCached {
    return [self fetchMediaFromCache].count > 0;
}

- (UserRealmObject *)fetchUserFromCache {
    RLMRealm *realm = [RLMRealm db];
    return [UserRealmObject allObjectsInRealm:realm].firstObject;
}

- (RLMResults<MediaRealmObject *> *)fetchMediaFromCache {
    RLMRealm *realm = [RLMRealm db];
    RLMResults<MediaRealmObject *> *results = [MediaRealmObject allObjectsInRealm:realm];
    [results sortedResultsUsingKeyPath:@"order" ascending:false];
    return [MediaRealmObject allObjectsInRealm:realm];
}

- (void)writeUserToCache:(UserRealmObject *)user {
    RLMRealm *realm = [RLMRealm db];
    [realm transactionWithBlock:^{
        [realm addObject:user];
    }];
}

- (void)writeMediaToCache:(NSArray<MediaRealmObject *> *)media {
    RLMRealm *realm = [RLMRealm db];
    [realm transactionWithBlock:^{
        [realm addObjects:media];
    }];
}

- (void)updateMediaCache:(NSArray<MediaRealmObject *> *)media {
    RLMRealm *realm = [RLMRealm db];
    RLMResults<MediaRealmObject *> *cachedMedia = [MediaRealmObject allObjectsInRealm:realm];
    [cachedMedia sortedResultsUsingKeyPath:@"order" ascending:true];
    
    NSUInteger numberOfNewObjects = media.count;
    
    NSMutableArray<MediaRealmObject *> *oldMedia = [[NSMutableArray<MediaRealmObject *> alloc] init];
    for (int i = 0; i < numberOfNewObjects; i++) {
        [oldMedia addObject:cachedMedia[i]];
    }
    
    [realm transactionWithBlock:^{
        [realm deleteObjects:oldMedia];
        [realm addObjects:media];
    }];
}

@end
