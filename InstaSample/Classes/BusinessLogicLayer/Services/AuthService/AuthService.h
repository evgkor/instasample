#import <Foundation/Foundation.h>
#import "UserRealmObject.h"

@protocol AuthService <NSObject>

- (void)authorize:(void (^)(UserRealmObject *))completion;

@end
