#import <AFNetworking/AFNetworking.h>
#import "AuthServiceImplementation.h"
#import "Constants.h"
#import "Endpoints.h"
#import "Helpers.h"
#import "SessionService.h"

@implementation AuthServiceImplementation

- (void)authorize:(void (^)(UserRealmObject *))completion {
    printFunction(_cmd);
    // TODO: Передать токен через адаптер
    NSString *urlString = [NSString stringWithFormat:@"%@%@?access_token=%@", kBaseURL, kEndpointUserInfo, SessionService.sharedInstance.accessToken];
    NSLog(@"urlString: %@", urlString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary *attributes = [responseObject valueForKeyPath:@"data"];
        UserRealmObject *user = [[UserRealmObject alloc] initWithAttributes:attributes];
        completion(user);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        // TODO: Обработать ошибку
        // TODO: Обработать истечение токена
        completion(nil);
    }];
}

@end
