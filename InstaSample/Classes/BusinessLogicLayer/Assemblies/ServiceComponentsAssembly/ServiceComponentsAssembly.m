#import "ServiceComponentsAssembly.h"
#import "AuthServiceImplementation.h"
#import "MediaServiceImplementation.h"
#import "MediaCacheServiceImplementation.h"

@implementation ServiceComponentsAssembly

- (id <AuthService>)authService {
    return [[AuthServiceImplementation alloc] init];
}

- (id <MediaService>)mediaService {
    return [[MediaServiceImplementation alloc] init];
}

- (id<MediaCacheService>)mediaCacheService {
    return [[MediaCacheServiceImplementation alloc] init];
}

@end
