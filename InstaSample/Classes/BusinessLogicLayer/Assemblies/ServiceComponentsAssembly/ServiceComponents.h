#import <Foundation/Foundation.h>
#import "AuthService.h"
#import "MediaService.h"
#import "MediaCacheService.h"

@protocol ServiceComponents <NSObject>

- (id<AuthService>)authService;
- (id<MediaService>)mediaService;
- (id<MediaCacheService>)mediaCacheService;

@end
