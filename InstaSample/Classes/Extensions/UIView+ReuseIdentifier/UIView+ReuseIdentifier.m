#import "UIView+ReuseIdentifier.h"

@implementation UIView (ReuseIdentifier)

+ (NSString *)reuseIdentifier {
    return [NSString stringWithFormat:@"%@", [self class]];
}

@end
