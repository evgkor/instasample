#import <UIKit/UIKit.h>

@interface UIView (ReuseIdentifier)

+ (NSString *)reuseIdentifier;

@end
