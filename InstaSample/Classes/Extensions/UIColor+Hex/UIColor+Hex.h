#import <UIKit/UIKit.h>

@interface UIColor (Hex)

// takes @"#123456"
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
