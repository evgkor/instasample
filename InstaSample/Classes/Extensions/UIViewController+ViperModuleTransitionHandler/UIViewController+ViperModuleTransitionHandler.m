#import "UIViewController+ViperModuleTransitionHandler.h"

@implementation UIViewController (ViperModuleTransitionHandler)

- (void)openModule:(UIViewController *)controller animated:(BOOL)flag {
    if(self.navigationController != nil) {
        [self.navigationController pushViewController:controller animated:flag];
    }
}

- (void)presentModule:(UIViewController *)controller animated:(BOOL)flag {
    [self presentViewController:controller animated:flag completion:nil];
}

@end
