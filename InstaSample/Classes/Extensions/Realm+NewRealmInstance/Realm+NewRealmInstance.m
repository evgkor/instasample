#import "Realm+NewRealmInstance.h"

@implementation RLMRealm (NewRealmInstance)

+ (UInt64)version {
    NSDictionary<NSString *, id> *dictionary = NSBundle.mainBundle.infoDictionary;
    NSString *version = dictionary[@"CFBundleShortVersionString"];
    NSString *build = dictionary[@"CFBundleVersion"];
    UInt64 schemaVersion = (UInt64)build.intValue;
//    int i = 0;
//    NSArray<NSString *> *components = [version componentsSeparatedByString:@"."];
//    for(NSString *versionPart in [components reverseObjectEnumerator]) {
//        i += 1;
//        UInt64 versionPartValue = (UInt64)versionPart.intValue;
//        schemaVersion += versionPartValue * (UInt64)(10000 ^ i);
//    }
    return schemaVersion;
}

+ (RLMRealm *)db {
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = [RLMRealm version];
    config.deleteRealmIfMigrationNeeded = true;
    NSError *error;
    return [RLMRealm realmWithConfiguration:config error:&error];
}

@end
