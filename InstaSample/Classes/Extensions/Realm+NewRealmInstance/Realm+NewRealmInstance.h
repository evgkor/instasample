#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface RLMRealm (NewRealmInstance)

+ (UInt64)version;
+ (RLMRealm *)db;

@end
