#import <UIKit/UIKit.h>

@protocol ViperModuleTransitionHandlerProtocol <NSObject>

- (void)openModule:(UIViewController*)controller animated:(BOOL)flag;
- (void)presentModule:(UIViewController*)controller animated:(BOOL)flag;

@end
