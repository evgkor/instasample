#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface LoginView : UIView

@property (nonatomic, strong) WKWebView* webView;

@end
