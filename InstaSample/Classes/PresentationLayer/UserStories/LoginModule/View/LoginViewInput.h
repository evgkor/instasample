#import <Foundation/Foundation.h>

@protocol LoginViewInput <NSObject>

- (void)setupWebViewWithURL:(NSURL *)url;
- (void)dismiss;

@end
