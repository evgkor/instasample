#import "LoginView.h"

@implementation LoginView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero];
        [self addSubview: _webView];
        
        [self makeConstraints];
    }
    return self;
}

#pragma mark - Private methods

- (void)makeConstraints {
    _webView.translatesAutoresizingMaskIntoConstraints = false;
    [_webView.topAnchor constraintEqualToAnchor:self.topAnchor].active = true;
    [_webView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = true;
    [_webView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = true;
    [_webView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = true;
}

@end
