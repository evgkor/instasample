#import <UIKit/UIKit.h>
#import "LoginViewInput.h"
#import "LoginViewOutput.h"
#import "LoginView.h"

@interface LoginViewController : UIViewController <LoginViewInput, WKNavigationDelegate>

@property (nonatomic, strong) id<LoginViewOutput> output;

@end
