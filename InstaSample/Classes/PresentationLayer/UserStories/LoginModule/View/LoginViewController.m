#import "LoginViewController.h"
#import "LoginView.h"
#import "Helpers.h"

@implementation LoginViewController

-(LoginView*)_view {
    return (LoginView *)self.view;
}

-(WKWebView*)webView {
    return [self _view].webView;
}

#pragma mark - Lifecycle

- (void)loadView {
    self.view = [[LoginView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _view].webView.navigationDelegate = self;
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - LoginViewInput

- (void)setupWebViewWithURL:(NSURL *)url {
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [[self _view].webView loadRequest:request];
}

- (void)dismiss {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:true completion:nil];
    });
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    [self.output didTriggerNavigationEvent:navigationAction decisionHandler:decisionHandler];
}

@end
