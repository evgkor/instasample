#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@protocol LoginViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void)didTriggerNavigationEvent:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler;

@end
