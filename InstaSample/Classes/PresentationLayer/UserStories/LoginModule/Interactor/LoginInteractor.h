#import <Foundation/Foundation.h>
#import "LoginInteractorInput.h"
#import "LoginInteractorOutput.h"
#import "AuthService.h"
#import "MediaCacheService.h"

@interface LoginInteractor : NSObject<LoginInteractorInput>

@property (nonatomic, weak) id<LoginInteractorOutput> output;
@property (nonatomic, strong) id <AuthService> authService;
@property (nonatomic, strong) id <MediaCacheService> cacheService;

@end
