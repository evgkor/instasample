#import "LoginInteractor.h"
#import "Constants.h"
#import "Endpoints.h"
#import "Helpers.h"
#import "SessionService.h"

@implementation LoginInteractor

- (NSURL *)obtainAuthURL {
    printFunction(_cmd);
    NSString *urlString = [NSString stringWithFormat:@"%@%@/?client_id=%@&redirect_uri=%@&response_type=token", kBaseURL, kEndpointAuthorize, kClientID, kRedirectURL];
    NSLog(@"urlString: %@", urlString);
    return [NSURL URLWithString:urlString];
}

// Example URL: http://yourcallback.com/#access_token=2198211652.5181f21.4a041ccd0ece4fa085395f8a892bd5f0
- (NSString *)extractToken:(NSURL *)url {
    printFunction(_cmd);
    NSString *token = [self extractValue:url param:@"access_token=" separator:@"#"];
    if(token != nil) {
        NSLog(@"access_token: %@", token);
        SessionService.sharedInstance.accessToken = token;
    }
    return token;
}

// Example URL: http://yourcallback.com/?error_reason=user_denied&error=access_denied&error_description=The+user+denied+your+request
- (NSString *)extractError:(NSURL *)url {
    printFunction(_cmd);
    return [self extractValue:url param:@"error=" separator:@"&"];
}

- (void)authorize {
    printFunction(_cmd);
    [self.authService authorize:^(UserRealmObject *response) {
        if(response != nil) {
            if(![self.cacheService isUserCached]) {
                [self.cacheService writeUserToCache:response];
            }
            
            [self.output didAuthorize];
        } else {
            [self.output didAuthorizeWithError];
        }
    }];
}

#pragma mark - Private methods

- (NSString *)extractValue:(NSURL *)url param:(NSString *)param separator:(NSString *)separator {
    NSString *value = nil;
    NSString *urlQuery = nil;
    if(url.query != nil) {
        urlQuery = url.query;
    } else {
        urlQuery = url.relativeString;
    }
    
    NSArray<NSString *> *components = [urlQuery componentsSeparatedByString:separator];
    for (NSString *comp in components) {
        NSRange range = [comp rangeOfString:param];
        if(range.length > 0) {
            value = [comp componentsSeparatedByString:@"="].lastObject;
        }
    }
    
    return value;
}

@end
