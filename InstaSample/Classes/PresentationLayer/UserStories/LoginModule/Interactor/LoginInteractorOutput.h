#import <Foundation/Foundation.h>

@protocol LoginInteractorOutput <NSObject>

- (void)didAuthorize;
- (void)didAuthorizeWithError;

@end
