#import <Foundation/Foundation.h>

@protocol LoginInteractorInput <NSObject>

- (NSURL *)obtainAuthURL;
- (NSString *)extractToken:(NSURL *)url;
- (NSString *)extractError:(NSURL *)url;
- (void)authorize;

@end
