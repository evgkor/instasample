#import <Foundation/Foundation.h>
#import "LoginRouterInput.h"
#import "ViperModuleTransitionHandlerProtocol.h"

@interface LoginRouter : NSObject<LoginRouterInput>

@property (nonatomic, weak) id<ViperModuleTransitionHandlerProtocol> transitionHandler;

@end
