#import <Foundation/Foundation.h>
#import "LoginViewInput.h"
#import "LoginViewOutput.h"
#import "LoginInteractorInput.h"
#import "LoginInteractorOutput.h"
#import "LoginModuleInput.h"
#import "LoginRouterInput.h"

@interface LoginPresenter : NSObject<LoginModuleInput, LoginViewOutput, LoginInteractorOutput>

@property (nonatomic, weak) id<LoginViewInput> view;
@property (nonatomic, strong) id<LoginInteractorInput>  interactor;
@property (nonatomic, strong) id<LoginRouterInput> router;
// @property (nonatomic, strong) LoginPresenterStateStorage *stateStorage;

@end
