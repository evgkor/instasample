#import "LoginPresenter.h"
#import "Constants.h"
#import "Helpers.h"

@implementation LoginPresenter

- (void)didTriggerViewReadyEvent {
    printFunction(_cmd);
    // Step One: Direct your user to our authorization URL
    NSURL *url = [self.interactor obtainAuthURL];
    [_view setupWebViewWithURL:url];    
}

- (void)didTriggerNavigationEvent:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    printFunction(_cmd);
    if(navigationAction.navigationType != WKNavigationTypeFormSubmitted && navigationAction.navigationType != -1) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    
    NSURL *url = navigationAction.request.URL;
    NSString *token = [self.interactor extractToken:url];
    NSString *error = [self.interactor extractError:url];
    
    if (token != nil) {
        // Step Two: Receive the access_token via the URL fragment
        [self.interactor authorize];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if (error != nil) {
        NSLog(@"error: %@", error);
        decisionHandler(WKNavigationActionPolicyCancel);
    } else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    
}

- (void)didAuthorize {
    printFunction(_cmd);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kUserLoginNotificationName
     object:self];
    [_view dismiss];
}

- (void)didAuthorizeWithError {
    printFunction(_cmd);
    NSAssert(false, @"Not implemented");
}

@end
