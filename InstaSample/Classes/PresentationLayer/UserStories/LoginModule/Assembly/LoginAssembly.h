#import <UIKit/UIKit.h>
#import "LoginModuleInput.h"
#import "ModuleAssemblyBase.h"

@interface LoginAssembly : ModuleAssemblyBase

- (UIViewController *)createModule: (void (^)(id<LoginModuleInput>))configuration;

@end
