#import "LoginAssembly.h"
#import "LoginViewController.h"
#import "LoginInteractor.h"
#import "LoginPresenter.h"
#import "LoginRouter.h"
#import "UIViewController+ViperModuleTransitionHandler.h"
#import "ServiceComponents.h"

@implementation LoginAssembly

- (UIViewController *)createModule: (void (^)(id<LoginModuleInput>))configuration {
    LoginViewController *view = [[LoginViewController alloc] init];
    LoginInteractor *interactor = [[LoginInteractor alloc] init];
    LoginPresenter *presenter = [[LoginPresenter alloc] init];
    LoginRouter *router = [[LoginRouter alloc] init];
    
    view.output = presenter;
    
    interactor.output = presenter;
    interactor.authService = self.serviceComponents.authService;
    interactor.cacheService = self.serviceComponents.mediaCacheService;
    
    presenter.view = view;
    presenter.interactor = interactor;
    presenter.router = router;
    
    router.transitionHandler = view;
    
    if(configuration) {
        configuration(presenter);
    }
    
    return view;
}

@end
