#import "MediaPresenter.h"
#import "Constants.h"
#import "Helpers.h"
#import "SessionService.h"

@implementation MediaPresenter

- (id) init {
    self = [super init];
    if (!self) return nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
        selector:@selector(handleNotification:)
        name:kUserLoginNotificationName
        object:nil];

    return self;
}

- (void) handleNotification:(NSNotification *) notification {
    printFunction(_cmd);
    [self.interactor obtainMediaAndUpdateCache];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - MediaViewOutput

- (void)didTriggerViewReadyEvent {
    printFunction(_cmd);
    if(!SessionService.sharedInstance.isAuthorized) {
        [self.router openLoginModule];
    } else {
        [self.interactor obtainMediaAndUpdateCache];
    }
}

- (void)didTriggerPullToRefreshEvent {
    [self.interactor obtainMediaFromNetwork];
}


#pragma mark - MediaInteractorOutput

- (void)didObtainMedia:(NSArray<MediaPlainObject *> *)data {
    MediaCollectionViewModel *viewModel = [self.mapperViewModel mapMediaCollectionViewModelFromMediaPlainObject:data];
    self.stateStorage.mediaViewModel = viewModel;
    [self.view updateViewWithMedia:viewModel];
}

- (void)didObtainMediaWithError {
    NSAssert(false, @"Not implemented");
}

- (void)didChangeMediaCollection:(MediaCollectionChange *)change data:(NSArray<MediaPlainObject *> *)data {
    MediaCollectionViewModel *viewModel = [self.mapperViewModel mapMediaCollectionViewModelFromMediaPlainObject:data];
    self.stateStorage.mediaViewModel = viewModel;
    [self.view updateViewWithMedia:change viewModel:viewModel];
}

@end
