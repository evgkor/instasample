#import <Foundation/Foundation.h>
#import "MediaCollectionViewModel.h"

@interface MediaPresenterStateStorage : NSObject

@property (nonatomic, strong) MediaCollectionViewModel *mediaViewModel;

@end
