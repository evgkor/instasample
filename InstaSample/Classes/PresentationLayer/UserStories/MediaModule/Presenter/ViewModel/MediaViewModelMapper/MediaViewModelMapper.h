#import <Foundation/Foundation.h>
#import "MediaCollectionViewModel.h"
#import "MediaPlainObject.h"

@interface MediaViewModelMapper : NSObject

- (MediaCollectionViewModel *)mapMediaCollectionViewModelFromMediaPlainObject:(NSArray<MediaPlainObject *> *)data;

@end
