#import "MediaViewModelMapper.h"

@implementation MediaViewModelMapper

- (MediaCollectionViewModel *)mapMediaCollectionViewModelFromMediaPlainObject:(NSArray<MediaPlainObject *> *)data {
    MediaCollectionViewModel *viewModel = [[MediaCollectionViewModel alloc] init];
    viewModel.data = data;
    return viewModel;
}

@end
