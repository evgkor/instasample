#import <Foundation/Foundation.h>
#import "MediaPlainObject.h"

@interface MediaCollectionViewModel : NSObject

@property (nonatomic, copy) NSArray<MediaPlainObject *> *data;

@end
