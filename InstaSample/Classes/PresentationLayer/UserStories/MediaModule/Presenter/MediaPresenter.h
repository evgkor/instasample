#import <Foundation/Foundation.h>
#import "MediaViewInput.h"
#import "MediaViewOutput.h"
#import "MediaInteractorInput.h"
#import "MediaInteractorOutput.h"
#import "MediaModuleInput.h"
#import "MediaRouterInput.h"
#import "MediaPresenterStateStorage.h"
#import "MediaViewModelMapper.h"

@interface MediaPresenter : NSObject<MediaModuleInput, MediaViewOutput, MediaInteractorOutput>

@property (nonatomic, weak) id<MediaViewInput> view;
@property (nonatomic, strong) id<MediaInteractorInput>  interactor;
@property (nonatomic, strong) id<MediaRouterInput> router;
@property (nonatomic, strong) MediaPresenterStateStorage *stateStorage;
@property (strong, nonatomic) MediaViewModelMapper *mapperViewModel;

@end
