#import <UIKit/UIKit.h>
#import "MediaModuleInput.h"
#import "ModuleAssemblyBase.h"

@interface MediaAssembly : ModuleAssemblyBase

- (UIViewController *)createModule: (void (^)(id<MediaModuleInput>))configuration;

@end
