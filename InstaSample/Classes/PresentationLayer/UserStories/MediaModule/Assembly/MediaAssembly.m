#import "MediaAssembly.h"
#import "MediaViewController.h"
#import "MediaInteractor.h"
#import "MediaPresenter.h"
#import "MediaRouter.h"
#import "UIViewController+ViperModuleTransitionHandler.h"
#import "ServiceComponents.h"
#import "MediaPresenterStateStorage.h"

@implementation MediaAssembly

- (UIViewController *)createModule: (void (^)(id<MediaModuleInput>))configuration {
    MediaViewController *view = [[MediaViewController alloc] init];
    MediaInteractor *interactor = [[MediaInteractor alloc] init];
    MediaPresenter *presenter = [[MediaPresenter alloc] init];
    MediaRouter *router = [[MediaRouter alloc] init];
    
    view.output = presenter;
    
    interactor.output = presenter;
    interactor.mediaService = self.serviceComponents.mediaService;
    interactor.cacheService = self.serviceComponents.mediaCacheService;
    
    presenter.view = view;
    presenter.interactor = interactor;
    presenter.router = router;
    presenter.stateStorage = [[MediaPresenterStateStorage alloc] init];
    presenter.mapperViewModel = [[MediaViewModelMapper alloc] init];
    
    router.transitionHandler = view;
    
    if(configuration) {
        configuration(presenter);
    }
    
    return view;
}

@end
