#import <Foundation/Foundation.h>

@protocol MediaViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void)didTriggerPullToRefreshEvent;

@end
