#import <UIKit/UIKit.h>

@interface MediaView : UIView

@property (nonatomic, strong) UICollectionView* collectionView;

@end
