#import "MediaView.h"

@implementation MediaView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = UIColor.whiteColor;
        [_collectionView setAlwaysBounceVertical:true];
         _collectionView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 20.0, 0.0);
        [self addSubview: _collectionView];
        
        [self makeConstraints];
    }
    return self;
}

#pragma mark - Private methods

- (void)makeConstraints {
    _collectionView.translatesAutoresizingMaskIntoConstraints = false;
    [_collectionView.topAnchor constraintEqualToAnchor:self.topAnchor].active = true;
    [_collectionView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = true;
    [_collectionView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = true;
    [_collectionView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = true;
}

@end
