#import "MediaViewController.h"
#import "MediaView.h"
#import "Helpers.h"
#import "MediaCell.h"
#import "UIView+ReuseIdentifier.h"

@implementation MediaViewController

-(MediaView*)_view {
    return (MediaView *)self.view;
}

-(UICollectionView*)collectionView {
    return self._view.collectionView;
}

#pragma mark - Lifecycle

- (void)loadView {
    self.view = [[MediaView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavBar];
    [self setupCollectionView];
    [self setupRefreshControl];
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - MediaViewInput

- (void)updateViewWithMedia:(MediaCollectionViewModel *)viewModel {
    self.viewModel = viewModel;
    [self.collectionView reloadData];
}

- (void)updateViewWithMedia:(MediaCollectionChange *)change viewModel:(MediaCollectionViewModel *)viewModel {
    self.viewModel = viewModel;
    
    NSMutableArray<NSIndexPath *> *insertions = [NSMutableArray arrayWithCapacity:change.insertions.count];
    for(NSNumber *index in change.insertions) {
        [insertions addObject:[NSIndexPath indexPathForItem:index.intValue inSection:0]];
    }
    
    NSMutableArray<NSIndexPath *> *deletions = [NSMutableArray arrayWithCapacity:change.deletions.count];
    for(NSNumber *index in change.deletions) {
        [deletions addObject:[NSIndexPath indexPathForItem:index.intValue inSection:0]];
    }
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView deleteItemsAtIndexPaths:deletions];
        [self.collectionView insertItemsAtIndexPaths:insertions];
    } completion:^(BOOL finished) {
        
    }];
    
    [self.collectionView.refreshControl endRefreshing];
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = collectionView.bounds.size.width;
    CGFloat height = width * 0.75;
    return CGSizeMake(width, height + 45);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.viewModel == nil) {
        return 0;
    } else {
        return self.viewModel.data.count;
    }
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MediaCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MediaCell reuseIdentifier] forIndexPath:indexPath];
    MediaPlainObject *media = self.viewModel.data[indexPath.item];
    [cell configureWithMedia:media];
    return cell;
}

#pragma mark - Private methods

- (void)setupNavBar {
    self.navigationItem.title = @"Публикации";
}

- (void)setupCollectionView {
    [self.collectionView registerClass:[MediaCell class] forCellWithReuseIdentifier:[MediaCell reuseIdentifier]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)setupRefreshControl {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshCollection) forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
}

- (void)refreshCollection {
    [self.output didTriggerPullToRefreshEvent];
}

@end
