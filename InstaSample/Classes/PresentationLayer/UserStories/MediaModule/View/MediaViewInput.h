#import <Foundation/Foundation.h>
#import "MediaCollectionViewModel.h"
#import "MediaCollectionChange.h"

@protocol MediaViewInput <NSObject>

- (void)updateViewWithMedia: (MediaCollectionViewModel *)viewModel;
- (void)updateViewWithMedia: (MediaCollectionChange *)change viewModel:(MediaCollectionViewModel *)viewModel;

@end
