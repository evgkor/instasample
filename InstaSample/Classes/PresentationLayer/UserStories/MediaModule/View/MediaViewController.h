#import <UIKit/UIKit.h>
#import "MediaViewInput.h"
#import "MediaViewOutput.h"
#import "MediaView.h"
#import "MediaCollectionViewModel.h"

@interface MediaViewController : UIViewController <MediaViewInput, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) id<MediaViewOutput> output;
@property (nonatomic, strong) MediaView *_view;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) MediaCollectionViewModel *viewModel;

@end
