#import <UIKit/UIKit.h>
#import "MediaPlainObject.h"

@interface MediaCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *textLabel;

- (void)configureWithMedia:(MediaPlainObject *)media;

@end
