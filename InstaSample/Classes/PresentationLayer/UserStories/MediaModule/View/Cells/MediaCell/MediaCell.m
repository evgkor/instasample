#import <SDWebImage/SDWebImage.h>
#import "MediaCell.h"

@implementation MediaCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview: _imageView];
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _textLabel.textColor = UIColor.blackColor;
        _textLabel.numberOfLines = 1;
        _textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _textLabel.font = [UIFont systemFontOfSize:10];
        [self addSubview: _textLabel];
        
        [self makeConstraints];
    }
    return self;
}

- (void)configureWithMedia:(MediaPlainObject *)media {
    NSURL *url = [NSURL URLWithString:media.image.url];
    [_imageView sd_setImageWithURL:url];
    _textLabel.text = media.caption.text;
}

#pragma mark - Private methods

- (void)makeConstraints {
    _imageView.translatesAutoresizingMaskIntoConstraints = false;
    [_imageView.topAnchor constraintEqualToAnchor:self.topAnchor].active = true;
    [_imageView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = true;
    [_imageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = true;
    [_imageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = true;
    [_imageView.heightAnchor constraintEqualToAnchor:self.widthAnchor multiplier:0.75].active = true;
    
    _textLabel.translatesAutoresizingMaskIntoConstraints = false;
    [_textLabel.topAnchor constraintEqualToAnchor:_imageView.bottomAnchor constant:12].active = true;
    [_textLabel.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:12].active = true;
    [_textLabel.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-24].active = true;
    [_textLabel.heightAnchor constraintEqualToConstant:12].active = true;
}

@end
