#import "MediaRouter.h"
#import "LoginAssembly.h"

@implementation MediaRouter

- (void)openLoginModule {
    LoginAssembly *assembly = [[LoginAssembly alloc] init];
    UIViewController *controller = [assembly createModule:nil];
    [self.transitionHandler presentModule:controller animated:true];
}

@end
