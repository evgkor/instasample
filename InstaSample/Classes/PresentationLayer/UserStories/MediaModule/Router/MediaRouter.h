#import <Foundation/Foundation.h>
#import "MediaRouterInput.h"
#import "ViperModuleTransitionHandlerProtocol.h"

@interface MediaRouter : NSObject<MediaRouterInput>

@property (nonatomic, weak) id<ViperModuleTransitionHandlerProtocol> transitionHandler;

@end
