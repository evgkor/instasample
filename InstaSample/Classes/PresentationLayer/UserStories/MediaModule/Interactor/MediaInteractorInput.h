#import <Foundation/Foundation.h>

@protocol MediaInteractorInput <NSObject>

- (void)obtainMediaAndUpdateCache;
- (void)obtainMediaFromCache;
- (void)obtainMediaFromNetwork;

@end
