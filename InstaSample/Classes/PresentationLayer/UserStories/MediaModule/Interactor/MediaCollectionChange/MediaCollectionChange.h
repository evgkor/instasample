#import <Foundation/Foundation.h>

@interface MediaCollectionChange : NSObject

@property NSMutableArray<NSNumber *> *insertions;
@property NSMutableArray<NSNumber *> *deletions;

@end
