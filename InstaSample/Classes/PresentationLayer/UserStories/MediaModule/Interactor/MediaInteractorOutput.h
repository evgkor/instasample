#import <Foundation/Foundation.h>
#import "MediaPlainObject.h"
#import "MediaCollectionChange.h"

@protocol MediaInteractorOutput <NSObject>

- (void)didObtainMedia:(NSArray <MediaPlainObject *> *)data;
- (void)didObtainMediaWithError;
- (void)didChangeMediaCollection:(MediaCollectionChange *)change data:(NSArray <MediaPlainObject *> *)data;

@end
