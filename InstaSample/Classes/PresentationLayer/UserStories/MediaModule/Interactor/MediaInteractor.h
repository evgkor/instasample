#import <Foundation/Foundation.h>
#import "MediaInteractorInput.h"
#import "MediaInteractorOutput.h"
#import "MediaService.h"
#import "MediaCacheService.h"

@interface MediaInteractor : NSObject<MediaInteractorInput>

@property (nonatomic, weak) id<MediaInteractorOutput> output;
@property (nonatomic, strong) id <MediaService> mediaService;
@property (nonatomic, strong) id <MediaCacheService> cacheService;

@end
