#import "MediaInteractor.h"
#import "Constants.h"
#import "Endpoints.h"
#import "Helpers.h"

@implementation MediaInteractor

- (void)obtainMediaAndUpdateCache {
    if([self.cacheService isMediaCached]) {
        [self obtainMediaFromCache];
    }
    
    [self obtainMediaFromNetwork];
}

- (void)obtainMediaFromCache {
    NSAssert([self.cacheService isMediaCached], @"");
    RLMResults<MediaRealmObject *> *cachedMedia = [self.cacheService fetchMediaFromCache];
    NSArray<MediaPlainObject *> *plainObjects = [self convertObjectsFromResults:cachedMedia];
    [self.output didObtainMedia:plainObjects];
}

- (void)obtainMediaFromNetwork {
    [self.mediaService obtainMedia:^(NSArray<MediaRealmObject *> *media, NSError *error) {
        if(error == nil) {
            if([self isInitialChange]) {
                [self prepareObjectsForInitialChange:media];
                [self.cacheService writeMediaToCache:media];
                RLMResults<MediaRealmObject *> *cachedMedia = [self.cacheService fetchMediaFromCache];
                NSArray<MediaPlainObject *> *plainObjects = [self convertObjectsFromResults:cachedMedia];
                [self.output didObtainMedia:plainObjects];
            } else {
                NSArray<MediaRealmObject *> *newObjects = [self prepareObjectsForUpdateChange:media];
                [self.cacheService updateMediaCache:newObjects];
                RLMResults<MediaRealmObject *> *cachedMedia = [self.cacheService fetchMediaFromCache];
                NSArray<MediaPlainObject *> *plainObjects = [self convertObjectsFromResults:cachedMedia];
                MediaCollectionChange *change = [self calcDiff:newObjects];
                [self.output didChangeMediaCollection:change data:plainObjects];
            }
        } else {
            NSAssert(media != nil && media.count > 0, @"");
            // TODO: Обработать ошибку
            [self.output didObtainMediaWithError];
        }
    }];
}

#pragma mark - Private methods

- (MediaCollectionChange *)calcDiff:(NSArray<MediaRealmObject *> *)newMedia {
    NSAssert([self.cacheService isMediaCached], @"");
    NSUInteger numberOfNewObjects = newMedia.count;
    RLMResults<MediaRealmObject *> *cachedMedia = [self.cacheService fetchMediaFromCache];
    NSUInteger numberOfCachedObjects = cachedMedia.count;
    MediaCollectionChange *change = [[MediaCollectionChange alloc] init];
    for (int i = 0; i < numberOfNewObjects; i++) {
        [change.insertions addObject:[NSNumber numberWithInt:i]];
        int index = (int)(numberOfCachedObjects - i - 1);
        [change.deletions addObject:[NSNumber numberWithInt:index]];
    }
    return change;
}

- (void)prepareObjectsForInitialChange:(NSArray<MediaRealmObject *> *)media {
    NSArray* reversedMedia = [[media reverseObjectEnumerator] allObjects];
    [self orderObjects:reversedMedia startOrder:0];
}

- (NSArray<MediaRealmObject *> *)prepareObjectsForUpdateChange:(NSArray<MediaRealmObject *> *)media {
    RLMResults<MediaRealmObject *> *cachedMedia = [self.cacheService fetchMediaFromCache];
    NSMutableArray<MediaRealmObject *> *newMedia = [[NSMutableArray<MediaRealmObject *> alloc] init];
    
    for(MediaRealmObject *object in media) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", object.id];
        NSUInteger index = [cachedMedia indexOfObjectWithPredicate:predicate];
        if(index == NSNotFound) {
            [newMedia addObject:object];
        }
    }
    
    NSArray* reversedMedia = [[newMedia reverseObjectEnumerator] allObjects];
    [self orderObjects:reversedMedia startOrder:cachedMedia.lastObject.order + 1];
    
    return reversedMedia;
}

- (void)orderObjects:(NSArray<MediaRealmObject *> *)media startOrder:(NSInteger)startOrder {
    for (int i = 0; i < media.count; i++) {
        media[i].order = startOrder + i;
    }
}

- (BOOL)isInitialChange {
    return ![self.cacheService isMediaCached];
}

- (NSArray<MediaPlainObject *> *)convertObjectsFromArray:(NSArray<MediaRealmObject *> *)data {
    NSMutableArray *plainObjects = [NSMutableArray arrayWithCapacity:data.count];
    for(MediaRealmObject * realmObject in data) {
        MediaPlainObject* mediaPlainObject = [[MediaPlainObject alloc] init];
        mediaPlainObject.id = realmObject.id;
        
        MediaImagePlainObject *mediaImagePlainObject = [[MediaImagePlainObject alloc] init];
        mediaImagePlainObject.id = realmObject.id;
        mediaImagePlainObject.width = realmObject.image.width;
        mediaImagePlainObject.height = realmObject.image.height;
        mediaImagePlainObject.url = realmObject.image.url;
        mediaPlainObject.image = mediaImagePlainObject;
        
        MediaCaptionPlainObject *mediaCaptionPlainObject = [[MediaCaptionPlainObject alloc] init];
        mediaCaptionPlainObject.id = realmObject.caption.id;
        mediaCaptionPlainObject.text = realmObject.caption.text;
        mediaPlainObject.caption = mediaCaptionPlainObject;
        
        [plainObjects addObject:mediaPlainObject];
    }
    
    NSAssert(data.count == plainObjects.count, @"");
    return plainObjects;
}

- (NSArray<MediaPlainObject *> *)convertObjectsFromResults:(RLMResults<MediaRealmObject *> *)data {
    NSMutableArray *mediaArray = [[NSMutableArray alloc] init];
    for (MediaRealmObject *object in data) {
        [mediaArray addObject:object];
    }
    return [self convertObjectsFromArray:mediaArray];
}

@end
