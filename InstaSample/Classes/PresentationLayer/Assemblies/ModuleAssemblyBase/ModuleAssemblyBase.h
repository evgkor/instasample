#import <Foundation/Foundation.h>

@protocol ServiceComponents;

@interface ModuleAssemblyBase: NSObject

@property (nonatomic, strong, readonly) id <ServiceComponents> serviceComponents;

@end
