#import "ModuleAssemblyBase.h"
#import "ServiceComponentsAssembly.h"

@implementation ModuleAssemblyBase

- (id <ServiceComponents>)serviceComponents {
    return [[ServiceComponentsAssembly alloc] init];
}

@end
