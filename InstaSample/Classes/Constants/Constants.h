#import <Foundation/Foundation.h>

extern NSString *const kBaseURL;
extern NSString *const kClientID;
extern NSString *const kRedirectURL;
extern NSString *const kUserLoginNotificationName;
