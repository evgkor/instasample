#import "Constants.h"

NSString *const kBaseURL = @"https://api.instagram.com";
NSString *const kClientID = @"5181f217305d45519e98ea6099e31d99";
NSString *const kRedirectURL = @"http://yourcallback.com/";
NSString *const kUserLoginNotificationName = @"userLoginNotification";
