#import "Endpoints.h"

NSString *const kEndpointAuthorize = @"/oauth/authorize";
NSString *const kEndpointUserInfo = @"/v1/users/self";
NSString *const kEndpointMediaRecent = @"/v1/users/self/media/recent";
