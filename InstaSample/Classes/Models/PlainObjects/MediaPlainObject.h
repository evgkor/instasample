#import <Foundation/Foundation.h>
#import "MediaImagePlainObject.h"
#import "MediaCaptionPlainObject.h"

@interface MediaPlainObject : NSObject

@property NSString *id;
@property MediaImagePlainObject *image;
@property MediaCaptionPlainObject *caption;

@end
