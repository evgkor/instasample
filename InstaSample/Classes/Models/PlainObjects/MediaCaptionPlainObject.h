#import <Foundation/Foundation.h>

@interface MediaCaptionPlainObject : NSObject

@property NSString *id;
@property NSString *text;

@end
