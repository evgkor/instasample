#import <Foundation/Foundation.h>

@interface MediaImagePlainObject : NSObject

@property NSString *id;
@property NSNumber *width;
@property NSNumber *height;
@property NSString *url;

@end
