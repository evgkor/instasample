#import <Foundation/Foundation.h>

@interface UserPlainObject : NSObject

@property NSString *id;
@property NSString *username;

@end
