#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "MediaImageRealmObject.h"
#import "MediaCaptionRealmObject.h"

@interface MediaRealmObject : RLMObject

@property NSString *id;
@property MediaImageRealmObject *image;
@property MediaCaptionRealmObject *caption;
@property NSInteger order;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
