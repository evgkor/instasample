#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface UserRealmObject : RLMObject

@property NSString *id;
@property NSString *username;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
