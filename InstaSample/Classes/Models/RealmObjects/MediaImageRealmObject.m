#import "MediaImageRealmObject.h"

@implementation MediaImageRealmObject

+ (NSString *)primaryKey {
    return @"id";
}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.id = [[NSUUID UUID] UUIDString];
    self.width = [attributes valueForKeyPath:@"width"];
    self.height = [attributes valueForKeyPath:@"height"];
    self.url = [attributes valueForKeyPath:@"url"];
    
    return self;
}

@end
