#import "MediaRealmObject.h"

@implementation MediaRealmObject

+ (NSString *)primaryKey {
    return @"id";
}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];
    
    NSDictionary *imagesAttributes = [attributes valueForKeyPath:@"images"];
    NSDictionary *standardImageAttributes = [imagesAttributes valueForKeyPath:@"standard_resolution"];
    self.image = [[MediaImageRealmObject alloc] initWithAttributes:standardImageAttributes];
    
    NSDictionary *captionAttributes = [attributes valueForKeyPath:@"caption"];
    self.caption = [[MediaCaptionRealmObject alloc] initWithAttributes:captionAttributes];
    
    self.order = 0;
    
    return self;
}

@end
