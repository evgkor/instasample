#import "MediaCaptionRealmObject.h"

@implementation MediaCaptionRealmObject

+ (NSString *)primaryKey {
    return @"id";
}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];
    self.text = [attributes valueForKeyPath:@"text"];
    
    return self;
}

@end
