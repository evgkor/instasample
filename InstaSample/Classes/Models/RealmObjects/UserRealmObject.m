#import "UserRealmObject.h"

@implementation UserRealmObject

+ (NSString *)primaryKey {
    return @"id";
}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.id = [attributes valueForKeyPath:@"id"];
    self.username = [attributes valueForKeyPath:@"username"];
    
    return self;
}

@end
