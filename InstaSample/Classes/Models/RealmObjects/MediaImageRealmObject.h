#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface MediaImageRealmObject : RLMObject

@property NSString *id;
@property NSNumber<RLMInt> *width;
@property NSNumber<RLMInt> *height;
@property NSString *url;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
