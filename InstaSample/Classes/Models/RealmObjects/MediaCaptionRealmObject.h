#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface MediaCaptionRealmObject : RLMObject

@property NSString *id;
@property NSString *text;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
